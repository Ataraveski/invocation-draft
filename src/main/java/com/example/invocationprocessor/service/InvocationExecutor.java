package com.example.invocationprocessor.service;


import com.example.invocationprocessor.controller.InvocationController;
import com.example.invocationprocessor.model.DataTransferDTO;
import com.example.invocationprocessor.model.Invocation;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class InvocationExecutor {

    Logger logger = LoggerFactory.getLogger(InvocationExecutor.class);
    final private PartnerDictionary partnerDictionary;

    public InvocationExecutor(PartnerDictionary partnerDictionary) {
        this.partnerDictionary = partnerDictionary;
    }


    public Invocation getInitInvocation(Long invocationId) {
        return this.partnerDictionary.getInvocation(invocationId);
    }


    public Pair<Invocation, DataTransferDTO> execute(Invocation invocation, DataTransferDTO request) {
        logger.info("invocation {}  request {}", invocation, request);
        var action = this.partnerDictionary.getInvocationImpl(invocation);
        if (!action.getConfiguration().equals(this.partnerDictionary.getConfiguration(invocation.getId()))) {
          //  throw new IllegalStateException("Wrong configuration state");
        }
        var response = action.call(request);
        var nextService = invocation.getNext() != null ?
                partnerDictionary.getInvocation(invocation.getNext()) : null;
        logger.info("next service {}  response {}", nextService, response);
        return new Pair<>(nextService, response);
    }


}
