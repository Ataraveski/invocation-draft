package com.example.invocationprocessor.service.impl;

import com.example.invocationprocessor.model.DataTransferDTO;
import com.example.invocationprocessor.model.InvocationConfiguration;
import com.example.invocationprocessor.service.ActionInvocation;

public class CreditInvocation implements ActionInvocation {
    private final InvocationConfiguration configuration;

    public CreditInvocation(InvocationConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public InvocationConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public DataTransferDTO call(DataTransferDTO request) {
        DataTransferDTO response = new DataTransferDTO();

        var amount = request.getLongByName("amount");
        var cardToken = request.getStringByName("token");
        var commission = request.getIntegerByName("commission");
        var transaction = request.getLongByName("transaction");

        if (amount != null && cardToken != null && commission != null) {
            //make request
            response.pushField("result", "OK");
            response.pushField("status", 0);
            response.pushField("transaction", transaction);
        }

        return response;
    }
}
