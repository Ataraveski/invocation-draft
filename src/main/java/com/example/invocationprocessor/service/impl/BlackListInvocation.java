package com.example.invocationprocessor.service.impl;

import com.example.invocationprocessor.model.*;
import com.example.invocationprocessor.service.ActionInvocation;

import java.util.Set;


public class BlackListInvocation implements ActionInvocation {

    private final InvocationConfiguration configuration;

    public BlackListInvocation(InvocationConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public InvocationConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public DataTransferDTO call(DataTransferDTO request) {
        var fio = request.getStringByName("FIO");

        DataTransferDTO response = new DataTransferDTO();

        if (fio != null && !fio.isEmpty()) {
            //check fio
            response.pushField("result", "OK");
            response.pushField("status", 0);
        } else {
            response.pushField("result", "BAD_REQUEST_DATA");
            response.pushField("status", -1);
        }

        return response;
    }
}
