package com.example.invocationprocessor.service.impl;

import com.example.invocationprocessor.model.DataTransferDTO;
import com.example.invocationprocessor.model.InvocationConfiguration;
import com.example.invocationprocessor.service.ActionInvocation;

public class InfoInvocation implements ActionInvocation {
    private final InvocationConfiguration configuration;

    public InfoInvocation(InvocationConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public InvocationConfiguration getConfiguration() {
        return this.configuration;
    }

    @Override
    public DataTransferDTO call(DataTransferDTO request) {
        DataTransferDTO response = new DataTransferDTO();
        var transactionId = request.getLongByName("transaction");
        if (transactionId != null && transactionId > 0) {
            response.pushField("FIO", "IVAN IVANOVICH");
            response.pushField("amount", 123456L);
            response.pushField("token", java.util.UUID.randomUUID().toString());
            response.pushField("commission", 1234);
            response.pushField("transaction", transactionId);
        } else {
            response.pushField("result", "TRANSACTION_NOT_FOUNT");
            response.pushField("status", -5);
        }

        return response;
    }
}
