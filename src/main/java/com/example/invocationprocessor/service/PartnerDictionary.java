package com.example.invocationprocessor.service;


import com.example.invocationprocessor.model.Invocation;
import com.example.invocationprocessor.model.InvocationConfiguration;
import com.example.invocationprocessor.service.impl.BlackListInvocation;
import com.example.invocationprocessor.service.impl.CreditInvocation;
import com.example.invocationprocessor.service.impl.InfoInvocation;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class PartnerDictionary {
    private Map<Long, Invocation> invocations = Map.of(
            1L, new Invocation(1L, "INFO", null, 1L, 2L, 1L),
            2L, new Invocation(2L, "ACTION", null, 2L, 3L, 1L),
            3L, new Invocation(3L, "APPLY", null, 3L, null, 1L)
    );
    private Map<Long, ActionInvocation> pckg = Map.of(1L, new InfoInvocation(new InvocationConfiguration()),
            2L, new BlackListInvocation(new InvocationConfiguration()),
            3L, new CreditInvocation(new InvocationConfiguration()));
    private Map<Long, InvocationConfiguration> invocationConfigurationMap = Map.of();

    public ActionInvocation getInvocationImpl(Invocation invocation) {
        return this.pckg.get(invocation.getPckg());
    }

    public Invocation getInvocation(Long invocationId) {
        return this.invocations.get(invocationId);
    }


    public InvocationConfiguration getConfiguration(Long configurationId) {
        return invocationConfigurationMap.get(configurationId);
    }
}
