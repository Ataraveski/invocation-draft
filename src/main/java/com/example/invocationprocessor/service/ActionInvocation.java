package com.example.invocationprocessor.service;

import com.example.invocationprocessor.model.DataTransferDTO;
import com.example.invocationprocessor.model.InvocationConfiguration;

public interface ActionInvocation {

    InvocationConfiguration getConfiguration();

    DataTransferDTO call(DataTransferDTO dataTransferDTO);
}
