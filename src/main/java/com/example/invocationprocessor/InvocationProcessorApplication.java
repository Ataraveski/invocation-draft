package com.example.invocationprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvocationProcessorApplication {

    public static void main(String[] args) {
        SpringApplication.run(InvocationProcessorApplication.class, args);
    }

}
