package com.example.invocationprocessor.controller;


import com.example.invocationprocessor.model.DataTransferDTO;
import com.example.invocationprocessor.service.InvocationExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController("")
public class InvocationController {

    Logger logger = LoggerFactory.getLogger(InvocationController.class);

    private final InvocationExecutor invocationExecutor;

    public InvocationController(InvocationExecutor invocationExecutor) {
        this.invocationExecutor = invocationExecutor;
    }

    @PostMapping("/act")
    public Map<String, Object> act(@RequestBody Map<String, Object> request) {
        var initInvocation =  this.invocationExecutor.getInitInvocation(1L);
        DataTransferDTO response = new DataTransferDTO();
        var pair = this.invocationExecutor.execute(initInvocation,  new DataTransferDTO(request));
        logger.info(String.valueOf(pair));
        while(pair.getKey() != null) {
            response.getFieldsMap().putAll(pair.getValue().getFieldsMap());
            pair =  this.invocationExecutor.execute(pair.getKey(), response);
            logger.info(String.valueOf(pair));
        }
        return pair.getValue() != null ? pair.getValue().getFieldsMap() : null;
    }
}
