package com.example.invocationprocessor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataTransferDTO {
    private Map<String, Object> fieldsMap = new HashMap<>();
  //  private Set<Field> fields;


    public void pushField(String name, Object field) {
        fieldsMap.put(name, field);
    }

    public Long getLongByName(String name){
        Long result = null;
        var value = fieldsMap.get(name);
        if (value instanceof Long) {
            result = (Long) value;
        } else if (value instanceof Number) {
            result = ((Number) value).longValue();
        }
        return result;
    }


    public Integer getIntegerByName(String name){
        Integer result = null;
        var value = fieldsMap.get(name);
        if (value instanceof Integer) {
            result = (Integer) value;
        }
        return result;
    }


    public String getStringByName(String name) {
        String result = null;
        var value = fieldsMap.get(name);
        if (value instanceof String) {
            result = (String) value;
        } else if (value != null){
            result = value.toString();
        }
        return result;
    }


//    public void addFields(Set<Field> newFields){
//        fields.addAll(newFields);
//    }
//
//    public Field getByFieldName(String name) {
//        return fields.stream().filter(el -> el.getName().equals(name))
//                .findFirst().orElse(null);
//    }
}
