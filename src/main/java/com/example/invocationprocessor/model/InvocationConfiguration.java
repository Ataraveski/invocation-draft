package com.example.invocationprocessor.model;


import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class InvocationConfiguration {
    private Long id;
    private Map<String, String> params = new HashMap<>();
}
