package com.example.invocationprocessor.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Invocation {
    private Long id;
    private String type;
    private Limit limit;
    private Long pckg;
    private Long next;
    private Long configuration;
}
